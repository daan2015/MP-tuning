""" The default strategy that iterates through the whole parameter space """
from __future__ import print_function

import itertools

from kernel_tuner import util

# Function that allows us to measure in parallel through pruning the configuration space until the given configuration
def prune_until_given_config(parameter_space, block_size_x, block_size_y, shmem_tile_size, x_elems_per_thread, y_elems_per_thread):
    count = 0

    for conf in parameter_space:
        count += 1
        if conf[0] == block_size_x and conf[1] == block_size_y and conf[2] == shmem_tile_size and conf[3] == x_elems_per_thread and conf[4] == y_elems_per_thread:
            break

    return parameter_space[count:]

def tune(runner, kernel_options, device_options, tuning_options):
    """ Tune all instances in the parameter space

    :params runner: A runner from kernel_tuner.runners
    :type runner: kernel_tuner.runner

    :param kernel_options: A dictionary with all options for the kernel.
    :type kernel_options: kernel_tuner.interface.Options

    :param device_options: A dictionary with all options for the device
        on which the kernel should be tuned.
    :type device_options: kernel_tuner.interface.Options

    :param tuning_options: A dictionary with all options regarding the tuning
        process.
    :type tuning_options: kernel_tuner.interface.Options

    :returns: A list of dictionaries for executed kernel configurations and their
        execution times. And a dictionary that contains a information
        about the hardware/software environment on which the tuning took place.
    :rtype: list(dict()), dict()

    """

    tune_params = tuning_options.tune_params
    restrictions = tuning_options.restrictions
    verbose = tuning_options.verbose

    #compute cartesian product of all tunable parameters
    if restrictions is None or isinstance(restrictions, list):
        parameter_space = itertools.product(*tune_params.values())
        #check for search space restrictions
        if restrictions is not None:
            parameter_space = filter(lambda p: util.check_restrictions(restrictions, p, tune_params.keys(), verbose), parameter_space)
    # Manually apply the restrictions in an optimized way for our use case (where we know when we can discard further configs without explicitly checking the restrictions on them)
    else:
        parameter_space = []
        values = list(tune_params.values())
        for block_size_x in values[0]:
            for block_size_y in values[1]:
                if block_size_x*block_size_y >= 32 and block_size_x*block_size_y%32==0:
                    for shmem_tile_size in values[2]:
                        if shmem_tile_size%block_size_x==0 or shmem_tile_size%block_size_y==0:
                            for x_elems_per_thread in values[3]:
                                if (shmem_tile_size*block_size_x*x_elems_per_thread + shmem_tile_size*block_size_y*1)*4/1024<=48:
                                    for y_elems_per_thread in values[4]:
                                        if (shmem_tile_size*block_size_x*x_elems_per_thread + shmem_tile_size*block_size_y*y_elems_per_thread)*4/1024<=48:
                                            parameter_space.append([block_size_x,block_size_y,shmem_tile_size,x_elems_per_thread,y_elems_per_thread])
                                        else:
                                            break
                                else:
                                    break

        # Ensure we start from the first not yet measured config.
        # if values[1][0] == 40:
            # parameter_space = prune_until_given_config(parameter_space, 8, 40, 8, 36, 2)
        # if values[0][0] == 12:
            # parameter_space = prune_until_given_config(parameter_space, 12, 8, 72, 11, 2)
        # if values[0][0] == 16:
            # parameter_space = prune_until_given_config(parameter_space, 16, 8, 8, 33, 26)
        # if values[0][0] == 24:
            # parameter_space = prune_until_given_config(parameter_space, 24, 32, 72, 3, 2)
        # if values[0][0] == 32:
            # parameter_space = prune_until_given_config(parameter_space, 32, 8, 16, 14, 12)
        # if values[0][0] == 96:
            # parameter_space = prune_until_given_config(parameter_space, 96, 9, 9, 8, 6)

    results, env = runner.run(parameter_space, kernel_options, tuning_options)

    return results, env
