#define WARP_SIZE 32
#define TOTAL_THREADS grid_div_x * block_size_x

#ifndef vector
#define vector 1
#endif

#if (vector == 1)
#define FLOAT_VECTOR float
#define END array_size
#define OUT_OF_L1_ITERS 6144 / WARP_SIZE
#elif (vector == 2)
#define FLOAT_VECTOR float2
#define END array_size / vector
#define OUT_OF_L1_ITERS 6144 / WARP_SIZE / vector
#elif (vector == 4)
#define FLOAT_VECTOR float4
#define END array_size / vector
#define OUT_OF_L1_ITERS 6144 / WARP_SIZE / vector
#endif

#define L2_OFFSET OUT_OF_L1_ITERS * TOTAL_THREADS

__global__ void l2_bandwidth(FLOAT_VECTOR *A, float *B) {
    int global_thread_id = blockIdx.x * block_size_x + threadIdx.x;
    int l2_treshold = 0;
    float sum;

    #pragma unroll
    for (int i = 0; i < END; i += TOTAL_THREADS) {
        FLOAT_VECTOR v = A[i + global_thread_id];
        #if (vector==1)
        sum += v;
        #elif (vector == 2)
        sum += v.x + v.y;
        #elif (vector == 4)
        sum += v.x + v.y + v.z + v.w;
        #endif
        l2_treshold += 1;
        if (l2_treshold - OUT_OF_L1_ITERS > 0 && sum > 0.0) {
            FLOAT_VECTOR v = A[i + global_thread_id - L2_OFFSET];
            #if (vector==1)
            sum += v;
            #elif (vector == 2)
            sum += v.x + v.y;
            #elif (vector == 4)
            sum += v.x + v.y + v.z + v.w;
            #endif
        }
    }

    B[global_thread_id] = sum;
}
