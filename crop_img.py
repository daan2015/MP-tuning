import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import sys


def find_non_empty_row(img, row_iterator):
    found_non_empty_row = False

    for i in row_iterator:
        for j in range(len(img[i])):
            if not all(rgba == 1. for rgba in img[i][j]):
                print("i,j: " + str(i) + "," + str(j) + ". Value: " + str(img[i][j]))
                found_non_empty_row = True
                break

        if found_non_empty_row:
            break

    return i


def find_lbound_col(img):
    closest_lbound = len(img[0])

    for i in range(len(img)):
        for j in range(len(img[i])):
            if not all(rgba == 1. for rgba in img[i][j]):
                if j < closest_lbound:
                    closest_lbound = j
                break

    print("lbound: " + str(closest_lbound))
    return closest_lbound


def find_rbound_col(img):
    closest_rbound = 0

    for i in range(len(img)):
        for j in range(len(img[i]) - 1, -1, -1):
            if not all(rgba == 1. for rgba in img[i][j]):
                if j > closest_rbound:
                    closest_rbound = j
                break

    print("rbound: " + str(closest_rbound))
    return closest_rbound


if __name__ == "__main__":
    if (len(sys.argv) < 3):
        print("Usage: python3 temp.py <result_file_path> <dimensions>")
        print("Dimensions: 'v' (vertical), 'h' (horizontal), 'b' (both)")
        exit()

    img_path = sys.argv[1]

    img = mpimg.imread(img_path)

    if sys.argv[2] == "v" or sys.argv[2] == "b":
        start_row = find_non_empty_row(img, range(len(img)))
        end_row = find_non_empty_row(img, range(len(img) - 1, -1, -1))
        img = img[start_row:end_row+1]

    if sys.argv[2] == "h" or sys.argv[2] == "b":
        start_col = find_lbound_col(img)
        end_col = find_rbound_col(img)
        img = [row[start_col:end_col+1] for row in img]

    plt.imsave(img_path, img)
