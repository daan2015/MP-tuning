from sys import argv
import json


if __name__ == "__main__":
    if len(argv) < 3:
        print("Usage: python merge_json_results.py <result_file_json_1> <result_file_json_2>")
        print("Expected format of the result files: {'csv': ['config1_csvdata', 'config2_csvdata', ...]}")
        exit()

    with open(argv[1], 'r') as f:
        csvdata1 = json.load(f)

    with open(argv[2], 'r') as f:
        csvdata2 = json.load(f)

    csv_results1 = [row.split(',') for row in csvdata1['csv']]
    csv_results2 = [row.split(',') for row in csvdata2['csv']]

    for row1 in csv_results1:
        for row2 in csv_results2:
            if row1[:3] == row2[:3]:
                new_row = row1 + row2[3:]
                print("\"" + ','.join(new_row) + "\",")
