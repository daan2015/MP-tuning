#!/usr/bin/env python
import numpy
from sys import argv as commandline_params
from kernel_tuner import tune_kernel, run_kernel
from collections import OrderedDict

def get_matrix_size(kernel_name):
    return (4096, 4096)

def get_tune_args(kernel_name):
    A = numpy.random.randn(*get_matrix_size(kernel_name)).astype(numpy.float32)
    B = numpy.random.randn(*get_matrix_size(kernel_name)).astype(numpy.float32)
    C = numpy.zeros((4096,4096),dtype=numpy.float32)

    args = [C, A, B]

    return args

def get_tune_params(kernel_name):
    tune_params = OrderedDict()

    tune_params["block_size_x"] = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024]
    tune_params["block_size_y"] = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024]

    return tune_params

def get_perform_tuning(kernel_name):
    return True

def tune(kernel_name):
    problem_size = (4096, 4096)

    args = get_tune_args(kernel_name)

    tune_params = get_tune_params(kernel_name)

    perform_tuning = get_perform_tuning(kernel_name)

    if perform_tuning:
        results = tune_kernel(kernel_name, kernel_name + ".cl", problem_size, args, tune_params, iterations=3)
    else:
        results = run_kernel(kernel_name, kernel_name + ".cl", problem_size, args, tune_params)
        for i, result in enumerate(results[0]):
            print(str(i) + "," + str(result))

if __name__ == "__main__":
    kernel_name = "block_size"

    # Allow this script to be used for variations of the matmul naive kernel
    if len(commandline_params) > 1:
        kernel_name = commandline_params[1]

    print("Starting the tuning of kernel: " + kernel_name)

    tune(kernel_name)
