#define WIDTH 4096

__kernel void column_only_access(__global float *C, __global float *A, __global float *B) {
    int x = get_group_id(0) * block_size_x + get_local_id(0);
    int y = get_group_id(1) * block_size_y + get_local_id(1);
    float sum = 0.0;

    for (int k = 0; k < WIDTH; k++) {
        sum += A[k * WIDTH + x] * B[k * WIDTH + x];
    }

    C[y * WIDTH + x] = sum;
}
