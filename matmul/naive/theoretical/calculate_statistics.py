#!/usr/bin/env python
from csv import reader as csvreader
from sys import argv as commandline_params
from math import ceil, floor

MATRIX_DIMENSION = 4096
COALESCING_NUM_ELEMENTS = 32
MAX_THREADS_PER_SM = 2048
MAX_BLOCKS_PER_SM = 32
NUM_SM = 24

def get_blocks_to_fill_row(block_size_x):
    return MATRIX_DIMENSION / block_size_x


def get_active_block_set_size(block_size_x, block_size_y):
    return min(MAX_BLOCKS_PER_SM, floor(MAX_THREADS_PER_SM / (block_size_x * block_size_y))) * NUM_SM


def get_active_block_sets_to_fill_row(block_size_x, block_size_y):
    return get_blocks_to_fill_row(block_size_x) / get_active_block_set_size(block_size_x, block_size_y)


def get_rows_per_active_block_set(block_size_x, block_size_y):
    return get_active_block_set_size(block_size_x, block_size_y) / get_blocks_to_fill_row(block_size_x)


def get_row_access_memory_fetches(block_size_x, block_size_y):
    # For each row, fetch all elements with full coalescing
    row_memory_fetches = MATRIX_DIMENSION * int(MATRIX_DIMENSION / COALESCING_NUM_ELEMENTS)
    rows_per_active_block_set = get_rows_per_active_block_set(block_size_x, block_size_y)

    # If our active set of blocks only does the last row partly, we need extra
    # fetches for those rows
    if (rows_per_active_block_set > 1 and rows_per_active_block_set % 1 != 0):
        row_memory_fetches += (ceil(ceil(rows_per_active_block_set) * MATRIX_DIMENSION / rows_per_active_block_set) - MATRIX_DIMENSION) * int(MATRIX_DIMENSION / COALESCING_NUM_ELEMENTS)
    # We need the original fetches once for every set of blocks that work on the
    # same row (since between active sets it does not remain in the cache with
    # our MATRIX_DIMENSION of 4096)
    else:
        row_memory_fetches *= ceil(get_active_block_sets_to_fill_row(block_size_x, block_size_y))

    return row_memory_fetches


def get_col_access_memory_fetches(block_size_x, block_size_y):
    # Calculate how many y elements we can process at a time (these share their
    # input column elements) and multiply this with full colaesced fetches for
    # all those required input column elements)
    return (int(MATRIX_DIMENSION / COALESCING_NUM_ELEMENTS) * MATRIX_DIMENSION *
        ceil((MATRIX_DIMENSION / (block_size_y * max(1, get_rows_per_active_block_set(block_size_x, block_size_y))))))


def get_theoretical_occupancy(block_size_x, block_size_y):
    return get_active_block_set_size(block_size_x, block_size_y) / NUM_SM * (block_size_x * block_size_y) / MAX_THREADS_PER_SM * 100


def get_total_thread_blocks(block_size_x, block_size_y):
    return ceil(MATRIX_DIMENSION / block_size_x) * ceil(MATRIX_DIMENSION / block_size_y)


def get_total_thread_block_groups(block_size_x, block_size_y):
    return ceil(get_total_thread_blocks(block_size_x, block_size_y) / get_active_block_set_size(block_size_x, block_size_y))


def get_last_active_thread_block_group_fill(block_size_x, block_size_y):
    active_thread_block_set_size = get_active_block_set_size(block_size_x, block_size_y)
    total_thread_blocks = get_total_thread_blocks(block_size_x, block_size_y)

    leftover_thread_blocks = total_thread_blocks % active_thread_block_set_size

    if leftover_thread_blocks == 0:
        leftover_thread_blocks = active_thread_block_set_size

    return leftover_thread_blocks / active_thread_block_set_size * 100


def get_last_row_thread_blocks_percentage(block_size_x, block_size_y):
    if block_size_x == 16 and block_size_y in [44,46,48,50,52,56]:
        print(ceil(MATRIX_DIMENSION / block_size_x))
        print(get_total_thread_blocks(block_size_x, block_size_y))
    return ceil(MATRIX_DIMENSION / block_size_x) / get_total_thread_blocks(block_size_x, block_size_y) * 100


def get_last_row_thread_block_utilization(block_size_x, block_size_y):
    block_size_y_last_blocks = MATRIX_DIMENSION % block_size_y

    if block_size_y_last_blocks == 0:
        block_size_y_last_blocks = block_size_y

    return block_size_y_last_blocks * block_size_x / (block_size_y * block_size_x) * 100


def print_result_as_csv(results):
    to_print = ""

    for result in results:
        print(to_print)

        to_print = "\""

        for number in result:
            to_print += str(number) + ","

        # Replace the last comma with '",'
        to_print = to_print[:-1] + "\","

    # Don't print the final comma
    print(to_print[:-1])

if __name__ == "__main__":
    if len(commandline_params) < 3:
        print("Usage: python calculate_statistics.py <configuration-csv-file> <statistic_types>")
        print("The following statistic types are supported:\n" +
            "row_access_memory_fetches\n" +
            "column_access_memory_fetches\n" +
            "theoretical_occupancy\n" +
            "total_thread_block_groups")
        exit()

    # Get the block size configurations
    with open(commandline_params[1], 'r') as csvfile:
        reader = csvreader(csvfile)

        block_size_x_y = [[int(row[0]), int(row[1])] for row in reader]

    statistic_types = commandline_params[2:]

    if "row_access_memory_fetches" in statistic_types:
        for config in block_size_x_y:
            config.append(get_row_access_memory_fetches(config[0], config[1]))
    if "column_access_memory_fetches" in statistic_types:
        for config in block_size_x_y:
            config.append(get_col_access_memory_fetches(config[0], config[1]))
    if "theoretical_occupancy" in statistic_types:
        for config in block_size_x_y:
            config.append(get_theoretical_occupancy(config[0], config[1]))
    if "total_thread_block_groups" in statistic_types:
        for config in block_size_x_y:
            config.append(get_total_thread_block_groups(config[0], config[1]))
    if "last_active_thread_block_group_fill" in statistic_types:
        for config in block_size_x_y:
            config.append(get_last_active_thread_block_group_fill(config[0], config[1]))
    if "last_row_thread_blocks_percentage" in statistic_types:
        for config in block_size_x_y:
            config.append(get_last_row_thread_blocks_percentage(config[0], config[1]))
    if "last_row_thread_block_utilization" in statistic_types:
        for config in block_size_x_y:
            config.append(get_last_row_thread_block_utilization(config[0], config[1]))

    print_result_as_csv(block_size_x_y)
