from sys import argv
import json
import math


MATRIX_DIMENSION = 4096
BYTES_PER_ELEM = 4
MAX_THREAD_BLOCKS_PER_SM = 32
MAX_THREADS_PER_SM = 2048
MAX_SHMEM_PER_SM_BYTES = 98304
BARRIERS_PER_ITERATION = 2
MAX_REGISTERS_PER_BLOCK = 65536


def get_elems_per_thread(x_elems_per_thread, y_elems_per_thread):
    return x_elems_per_thread * y_elems_per_thread


def get_threads_per_block(block_size_x, block_size_y):
    return block_size_x * block_size_y


def get_elems_per_block(block_size_x, block_size_y, x_elems_per_thread, y_elems_per_thread):
    return get_threads_per_block(block_size_x, block_size_y) * get_elems_per_thread(x_elems_per_thread, y_elems_per_thread)


def get_registers_per_block(block_size_x, block_size_y, registers_per_thread):
    return get_threads_per_block(block_size_x, block_size_y) * registers_per_thread


def get_shmem_per_block(block_size_x, block_size_y, shmem_tile_size, x_elems_per_thread, y_elems_per_thread):
    return (block_size_x * x_elems_per_thread + block_size_y * y_elems_per_thread) * shmem_tile_size * BYTES_PER_ELEM


# Calculate the number of blocks per SM, taking into account the hardware occupancy, shared memory limits, and register limits
def get_blocks_per_sm(block_size_x, block_size_y, shmem_tile_size, x_elems_per_thread, y_elems_per_thread, registers_per_thread):
    num_blocks_normal = min(MAX_THREAD_BLOCKS_PER_SM, int(MAX_THREADS_PER_SM / get_threads_per_block(block_size_x, block_size_y)))
    shmem_per_block = get_shmem_per_block(block_size_x, block_size_y, shmem_tile_size, x_elems_per_thread, y_elems_per_thread)
    registers_per_block = get_registers_per_block(block_size_x, block_size_y, registers_per_thread)

    return min(min(int(MAX_SHMEM_PER_SM_BYTES / shmem_per_block), num_blocks_normal), int(MAX_REGISTERS_PER_BLOCK / registers_per_block))


def get_threads_per_sm(block_size_x, block_size_y, shmem_tile_size, x_elems_per_thread, y_elems_per_thread, registers_per_thread):
    return get_threads_per_block(block_size_x, block_size_y) * get_blocks_per_sm(block_size_x, block_size_y, shmem_tile_size, x_elems_per_thread, y_elems_per_thread, registers_per_thread)


def get_elems_per_sm(block_size_x, block_size_y, shmem_tile_size, x_elems_per_thread, y_elems_per_thread, registers_per_thread):
    return get_elems_per_block(block_size_x, block_size_y, x_elems_per_thread, y_elems_per_thread) * get_blocks_per_sm(block_size_x, block_size_y, shmem_tile_size, x_elems_per_thread, y_elems_per_thread, registers_per_thread)


def get_shmem_per_sm(block_size_x, block_size_y, shmem_tile_size, x_elems_per_thread, y_elems_per_thread, registers_per_thread):
    return get_shmem_per_block(block_size_x, block_size_y, shmem_tile_size, x_elems_per_thread, y_elems_per_thread) * get_blocks_per_sm(block_size_x, block_size_y, shmem_tile_size, x_elems_per_thread, y_elems_per_thread, registers_per_thread)


def get_registers_per_sm(block_size_x, block_size_y, shmem_tile_size, x_elems_per_thread, y_elems_per_thread, registers_per_thread):
    return get_blocks_per_sm(block_size_x, block_size_y, shmem_tile_size, x_elems_per_thread, y_elems_per_thread, registers_per_thread) * get_registers_per_block(block_size_x, block_size_y, registers_per_thread)


if __name__ == "__main__":
    if len(argv) < 2:
        print("Usage: python calculate_blocking_statistics.py <result_file> <statistics>")
        print("Expected format of result_file csv: 'block_size_x,block_size_y,shmem_tile_size,x_elems_per_thread,y_elems_per_thread,time(ms),registers_per_thread,...'")
        print("Supported statistics: threads_per_block shmem_per_block registers_per_block blocks_per_sm threads_per_sm shmem_per_sm registers_per_sm")
        exit()

    with open(argv[1], 'r') as f:
        data = json.load(f)

    csv_results = [row.split(',') for row in data['csv']]

    statistics_to_print = argv[2:]

    final_str = ""

    for row in csv_results:
        final_str += "\"" + ",".join(row)

        if "elems_per_thread" in statistics_to_print:
            final_str += "," + str(get_elems_per_thread(int(row[3]), int(row[4])))
        if "threads_per_block" in statistics_to_print:
            final_str += "," + str(get_threads_per_block(int(row[0]), int(row[1])))
        if "elems_per_block" in statistics_to_print:
            final_str += "," + str(get_elems_per_block(int(row[0]), int(row[1]), int(row[3]), int(row[4])))
        if "shmem_per_block" in statistics_to_print:
            final_str += "," + str(get_shmem_per_block(int(row[0]), int(row[1]), int(row[2]), int(row[3]), int(row[4])))
        if "registers_per_block" in statistics_to_print:
            final_str += "," + str(get_registers_per_block(int(row[0]), int(row[1]), int(row[6])))
        if "blocks_per_sm" in statistics_to_print:
            final_str += "," + str(get_blocks_per_sm(int(row[0]), int(row[1]), int(row[2]), int(row[3]), int(row[4]), int(row[6])))
        if "threads_per_sm" in statistics_to_print:
            final_str += "," + str(get_threads_per_sm(int(row[0]), int(row[1]), int(row[2]), int(row[3]), int(row[4]), int(row[6])))
        if "elems_per_sm" in statistics_to_print:
            final_str += "," + str(get_elems_per_sm(int(row[0]), int(row[1]), int(row[2]), int(row[3]), int(row[4]), int(row[6])))
        if "shmem_per_sm" in statistics_to_print:
            final_str += "," + str(get_shmem_per_sm(int(row[0]), int(row[1]), int(row[2]), int(row[3]), int(row[4]), int(row[6])))
        if "registers_per_sm" in statistics_to_print:
            final_str += "," + str(get_registers_per_sm(int(row[0]), int(row[1]), int(row[2]), int(row[3]), int(row[4]), int(row[6])))

        final_str += "\",\n"

    print(final_str[:-2])
