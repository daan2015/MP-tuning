#!/usr/bin/env python
import numpy
from sys import argv as commandline_params
from kernel_tuner import tune_kernel, run_kernel
from collections import OrderedDict

def get_matrix_size(kernel_name):
    if kernel_name == "register_blocking":
        return (4096, 4096)

def get_tune_args(kernel_name):
    A = numpy.random.randn(*get_matrix_size(kernel_name)).astype(numpy.float32)
    B = numpy.random.randn(*get_matrix_size(kernel_name)).astype(numpy.float32)
    C = numpy.zeros((4096,4096),dtype=numpy.float32)

    args = [C, A, B]

    return args

def get_tune_params(kernel_name):
    tune_params = OrderedDict()

    tune_params["block_size_x"] = [i for i in range(6,193)]
    tune_params["block_size_y"] = [i for i in range(3,129)]
    tune_params["shmem_tile_size"] = [i for i in range(8,471)]
    tune_params["x_elems_per_thread"] = [i for i in range(3,40)]
    tune_params["y_elems_per_thread"] = [i for i in range(2,40)]

    return tune_params

def get_tune_restrictions(kernel_name):
    restrictions = ["block_size_x*block_size_y>=32", "block_size_x*block_size_y%32==0", "shmem_tile_size%block_size_x==0 or shmem_tile_size%block_size_y==0", "(shmem_tile_size*block_size_x*x_elems_per_thread + shmem_tile_size*block_size_y*y_elems_per_thread)*4/1024<=48"]

    return restrictions

def get_tune_answer(kernel_name, tune_args):
    return [numpy.matmul(tune_args[1], tune_args[2]), None, None]

def tune(kernel_name):
    problem_size = (4096, 4096)

    args = get_tune_args(kernel_name)

    tune_params = get_tune_params(kernel_name)

    restrict = get_tune_restrictions(kernel_name)

    tune_answer = get_tune_answer(kernel_name, args)

    results = tune_kernel(kernel_name, kernel_name + ".cu", problem_size, args, tune_params, grid_div_x=["block_size_x", "x_elems_per_thread"], grid_div_y=["block_size_y", "y_elems_per_thread"], compiler_options=["-lineinfo"], iterations=1, atol=0.001, restrictions={}, answer=tune_answer)

if __name__ == "__main__":
    kernel_name = "register_blocking"

    # Allow this script to be used for variations of the kernel
    if len(commandline_params) > 1:
        kernel_name = commandline_params[1]

    tune(kernel_name)
