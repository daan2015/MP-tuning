#define WIDTH 4096
#define TOTAL_BLOCK_SIZE_X block_size_x * x_elems_per_thread
#define TOTAL_BLOCK_SIZE_Y block_size_y * y_elems_per_thread

__global__ void register_blocking(float *C, float *A, float *B) {
    __shared__ float sA[TOTAL_BLOCK_SIZE_Y][shmem_tile_size];
    __shared__ float sB[shmem_tile_size][TOTAL_BLOCK_SIZE_X];

    int tx = threadIdx.x;
    int ty = threadIdx.y;
    int x = blockIdx.x * TOTAL_BLOCK_SIZE_X + tx;
    int y = blockIdx.y * TOTAL_BLOCK_SIZE_Y + ty;

    float sum[x_elems_per_thread][y_elems_per_thread];
    int k, l, m, n, cur_shmem_tile_size, cur_x, cur_y;

    for (k = 0; k < x_elems_per_thread; k++) {
        for (l = 0; l < y_elems_per_thread; l++) {
            sum[k][l] = 0.0;
        }
    }

    cur_shmem_tile_size = shmem_tile_size;

    for (k = 0; k < WIDTH; k += shmem_tile_size) {
        if (k + shmem_tile_size > WIDTH) {
            cur_shmem_tile_size = WIDTH - k;
        }

        __syncthreads();
        for (l = 0; l < cur_shmem_tile_size; l += block_size_x) {
            for (m = 0; m < TOTAL_BLOCK_SIZE_Y; m += block_size_y) {
                if (l + tx < cur_shmem_tile_size) {
                    sA[ty + m][l + tx] = A[(y + m) * WIDTH + k + l + tx];
                }
            }
        }

        for (l = 0; l < cur_shmem_tile_size; l += block_size_y) {
            for (m = 0; m < TOTAL_BLOCK_SIZE_X; m += block_size_x) {
                if (l + ty < cur_shmem_tile_size) {
                    sB[l + ty][tx + m] = B[(k + l + ty) * WIDTH + x + m];
                }
            }
        }
        __syncthreads();

        for (l = 0; l < cur_shmem_tile_size; l++) {
            for (m = 0; m < x_elems_per_thread; m++) {
                for (n = 0; n < y_elems_per_thread; n++) {
                    sum[m][n] += sA[ty + n * block_size_y][l] * sB[l][tx + m * block_size_x];
                }
            }
        }
    }

    for (k = 0; k < x_elems_per_thread; k++) {
        cur_x = x + k * block_size_x;
        for (l = 0; l < y_elems_per_thread; l++) {
            cur_y = y + l * block_size_y;
            if (cur_x < WIDTH && cur_y < WIDTH) {
                C[cur_y * WIDTH + cur_x] = sum[k][l];
            }
        }
    }
}
